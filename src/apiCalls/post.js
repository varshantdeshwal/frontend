import url from "./url";

var result = async (route, data) => {
   const res = await fetch(
      url + route,

      {
         method: "POST",
         headers: {
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json"
         },
         body: JSON.stringify(data)
      }
   );

   const response = await res.json();
   return response;
};

export default result;
