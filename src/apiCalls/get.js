import url from "./url";

var result = async (route, param) => {
   const res = await fetch(
      url + route + "/" + param,

      {
         method: "GET"
      }
   );

   const response = await res.json();
   return response;
};

export default result;
