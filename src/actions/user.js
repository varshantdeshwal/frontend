import { get, post } from "../apiCalls";
export const verifyUser = ({ history, data }) => {
   return async dispatch => {
      const result = await post("user/login", {
         email: data.email,
         password: data.password
      });
      if (result.message === "authorized") {
         await localStorage.setItem("auth", true);
         await localStorage.setItem("email", data.email);
         await localStorage.setItem("name", result.doc.name);
         history.push("/home");
      } else {
         dispatch({
            type: "ON_UPDATE_USER",
            data: {
               info: result.message
            }
         });
      }
   };
};

export const signup = ({ history, data }) => {
   return async dispatch => {
      const result = await post("user/signup", {
         name: data.name,
         email: data.email,
         password: data.password
      });
      if (result.message === "success") {
         await localStorage.setItem("auth", true);
         await localStorage.setItem("email", data.email);
         await localStorage.setItem("name", result.doc.name);
         await history.push("/home");
      } else {
         dispatch({
            type: "ON_UPDATE_USER",
            data: {
               info: result.message
            }
         });
      }
   };
};
export const joinChannel = (name, email) => {
   return async dispatch => {
      await post("user/join-channel", {
         channelName: name,
         email
      });
      const result = await get("user/channels", email);
      dispatch({
         type: "ON_UPDATE_USER",
         data: {
            channels: result.doc
         }
      });
   };
};
export const getJoinedChannels = email => {
   return async dispatch => {
      const result = await get("user/channels", email);
      dispatch({
         type: "ON_UPDATE_USER",
         data: {
            channels: result.doc
         }
      });
   };
};

export const onFieldChange = (fieldName, fieldValue, type) => {
   return async dispatch => {
      dispatch({
         type,
         data: {
            [fieldName]: fieldValue
         }
      });
   };
};

export const populateStateFromLocal = history => {
   return async dispatch => {
      const email = localStorage.getItem("email");
      const name = localStorage.getItem("name");
      const auth = localStorage.getItem("auth");
      if (auth) {
         dispatch({
            type: "ON_UPDATE_USER",
            data: {
               auth,
               name,
               email,
               info: "",
               channels: []
            }
         });
      } else {
         history.push("/signin");
      }
   };
};
