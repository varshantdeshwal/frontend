import { get, post } from "../apiCalls";

export const addChannel = (name, purpose) => {
   return async dispatch => {
      await post("channel", { name, purpose });
      const result = await get("channel", "");
      dispatch({
         type: "ON_UPDATE_CHANNEL",
         data: { list: result.doc }
      });
   };
};
export const resetAddChannelForm = () => {
   return async dispatch => {
      dispatch({
         type: "ON_UPDATE_CHANNEL",
         data: { name: "", purpose: "" }
      });
   };
};
export const setActiveChannel = (key, fetchMessages) => {
   return async dispatch => {
      if (fetchMessages) {
         const result = await get("channel/get-messages", key);
         dispatch({
            type: "ON_UPDATE_CHANNEL",
            data: { messages: result.doc }
         });
      }
      dispatch({
         type: "ON_UPDATE_CHANNEL",
         data: { activeChannel: key }
      });
   };
};

export const getChannelList = () => {
   return async dispatch => {
      const result = await get("channel", "");
      dispatch({
         type: "ON_UPDATE_CHANNEL",
         data: { list: result.doc }
      });
   };
};
export const getMessages = name => {
   return async dispatch => {
      const result = await get("channel/get-messages", name);
      dispatch({
         type: "ON_UPDATE_CHANNEL",
         data: { messages: result.doc }
      });
   };
};
export const addMessage = (newMessage, channelName, sender) => {
   return async dispatch => {
      
      await post("channel/add-message", {
         name: channelName,
         message: newMessage,
         sender
      });
      const result = await get("channel/get-messages", channelName);
      dispatch({
         type: "ON_UPDATE_CHANNEL",
         data: { messages: result.doc, newMessage: "" }
      });
   };
};
