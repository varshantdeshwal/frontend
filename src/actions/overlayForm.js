export const toggleOverlayForm = () => {
   return async dispatch => {
      dispatch({
         type: "TOGGLE_OVERLAY_FORM"
      });
   };
};
