import React, { Component } from "react";
import { connect } from "react-redux";
import { onFieldChange, verifyUser } from "../actions";
import { TextInput, Label, SubmitButton, FormTitle } from "./common";
import { Link, withRouter } from "react-router-dom";
class SigninForm extends Component {
   state = {};
   onHandleChange = e => {
      this.props.onFieldChange(e.target.name, e.target.value, "ON_UPDATE_USER");
   };

   render() {
      return (
         <div className="center">
            <form
               className="form"
               onSubmit={e => {
                  e.preventDefault();
                  this.props.verifyUser({
                     history: this.props.history,
                     data: this.props.userData
                  });
               }}
            >
               <FormTitle label="Demo"> </FormTitle>
               <Label label="EMAIL" />
               <TextInput
                  placeholder="Enter your email"
                  name="email"
                  onChangeAction={e => this.onHandleChange(e)}
                  value={this.props.userData.email || ""}
               />
               <Label label="PASSWORD" />
               <TextInput
                  placeholder="Enter your password"
                  password
                  name="password"
                  onChangeAction={e => this.onHandleChange(e)}
                  value={this.props.userData.password || ""}
               />
               <SubmitButton name="login" title="Login" />
               <Link
                  to="/signup"
                  className="center"
                  onClick={() => {
                     this.props.onFieldChange("info", "", "ON_UPDATE_USER");
                  }}
               >
                  Signup
               </Link>
               <p className="center info">
                  {this.props.userData.info && this.props.userData.info !== ""
                     ? "*" + this.props.userData.info
                     : null}
               </p>
            </form>
         </div>
      );
   }
}
function mapStateToProps(state) {
   return {
      userData: state.user
   };
}
export default withRouter(
   connect(
      mapStateToProps,
      { onFieldChange, verifyUser }
   )(SigninForm)
);
