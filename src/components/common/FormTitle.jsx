import React from "react";

export default props => {
  return <div className="center form-title">{props.label}</div>;
};
