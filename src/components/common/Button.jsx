import React from "react";

export default props => {
   return (
      <div className="center">
         <button
            className="button"
            name={props.name}
            onClick={props.handleButtonClick}
            type="button"
            style={props.style || {}}
         >
            {props.title}
         </button>
      </div>
   );
};
