import React from "react";

export default props => {
  return (
    <div>
      <input
        className="input-field"
        placeholder={props.placeholder}
        onChange={e => props.onChangeAction(e)}
        name={props.name}
        type={props.password ? "password" : "text"}
        value={props.value}
        style={props.style || {}}
        required
      />
    </div>
  );
};
