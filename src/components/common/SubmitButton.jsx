import React from "react";

export default props => {
  return (
    <div className="center">
      <input
        className="submit-button"
        name={props.name}
        type="submit"
        value={props.title}
      />
    </div>
  );
};
