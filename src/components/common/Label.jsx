import React from "react";

export default props => {
  return (
    <div className="form-label">
      <label>{props.label}</label>
    </div>
  );
};
