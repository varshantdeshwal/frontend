import React, { Component } from "react";
import { connect } from "react-redux";
import { onFieldChange, signup } from "../actions";
import { TextInput, Label, FormTitle, SubmitButton } from "./common";
import { Link, withRouter } from "react-router-dom";
class SignupForm extends Component {
   state = {};
   onHandleChange = e => {
      this.props.onFieldChange(e.target.name, e.target.value, "ON_UPDATE_USER");
   };
   render() {
      return (
         <div className="center">
            <form
               className="form"
               onSubmit={e => {
                  e.preventDefault();
                  this.props.signup({
                     history: this.props.history,
                     data: this.props.userData
                  });
               }}
            >
               <FormTitle label="Demo"> </FormTitle>
               <Label label="NAME" />
               <TextInput
                  placeholder="Enter your name"
                  name="name"
                  onChangeAction={e => this.onHandleChange(e)}
                  value={this.props.userData.name || ""}
               />
               <Label label="EMAIL" />
               <TextInput
                  placeholder="Enter your email"
                  name="email"
                  onChangeAction={e => this.onHandleChange(e)}
                  value={this.props.userData.email || ""}
               />
               <Label label="PASSWORD" />
               <TextInput
                  placeholder="Enter your password"
                  password
                  name="password"
                  onChangeAction={e => this.onHandleChange(e)}
                  value={this.props.userData.password || ""}
               />
               <SubmitButton name="signup" title="Signup" />
               <Link
                  to="/signin"
                  className="center"
                  onClick={() => {
                     this.props.onFieldChange("info", "", "ON_UPDATE_USER");
                  }}
               >
                  Login
               </Link>
               <p className="center info">
                  {this.props.userData.info && this.props.userData.info !== ""
                     ? "*" + this.props.userData.info
                     : null}
               </p>
            </form>
         </div>
      );
   }
}
function mapStateToProps(state) {
   return {
      userData: state.user
   };
}
export default withRouter(
   connect(
      mapStateToProps,
      { onFieldChange, signup }
   )(SignupForm)
);
