import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect, withRouter } from "react-router-dom";
class StartScreen extends Component {
   render() {
      return localStorage.getItem("auth") ? (
         <Redirect to="/home" />
      ) : (
         <Redirect to="/signin" />
      );
   }
}

function mapStateToProps(state) {
   return {
      userAuth: state.user.auth
   };
}
export default withRouter(connect(mapStateToProps)(StartScreen));
