import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { populateStateFromLocal } from "../../actions";
import ChannelList from "./ChannelList";
class Dashboard extends Component {
   render() {
      return (
         <Fragment>
            <div className="user-signature">
               <h3>{this.props.userData.name}</h3>
               <div>
                  <i className="fas fa-envelope" />
                  <p>{this.props.userData.email}</p>
               </div>
            </div>
            <ChannelList />

            <div className="separator" />
            <Link
               to={"/signin"}
               className="channel-link logout"
               onClick={() => {
                  localStorage.clear();
                  this.populateStateFromLocal();
               }}
            >
               <div className="logout-link">Logout</div>
            </Link>
         </Fragment>
      );
   }
}
function mapStateToProps(state) {
   return {
      userData: state.user
   };
}
export default connect(
   mapStateToProps,
   { populateStateFromLocal }
)(Dashboard);
