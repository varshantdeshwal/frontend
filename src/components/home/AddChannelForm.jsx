import React, { Component } from "react";
import { connect } from "react-redux";
import io from "socket.io-client";
import {
  onFieldChange,
  toggleOverlayForm,
  addChannel,
  getChannelList,
  joinChannel,
  getJoinedChannels,
  setActiveChannel,
  getMessages,
  resetAddChannelForm
} from "../../actions";
import { TextInput, SubmitButton, Label, FormTitle, Button } from "../common";
class AddChannelForm extends Component {
  constructor(props){
    super(props);
    this.state = { info: "" };
    this.socket = io('localhost:3048');
    
  }
  
  onHandleChange = e => {
    this.props.onFieldChange(
      e.target.name,
      e.target.value,
      "ON_UPDATE_CHANNEL"
    );
  };
  add = async e => {
    e.preventDefault();
    
    if (!this.props.channelList.includes(this.props.name)) {
      await this.props.addChannel(this.props.name, this.props.purpose);
      await this.props.joinChannel(this.props.name, this.props.email);
      await this.props.getMessages(this.props.name);
      await this.props.setActiveChannel(this.props.name, true);
      await this.props.resetAddChannelForm();
      this.socket.emit('ADD_CHANNEL', {
        message:'channel added',
        email:this.props.email
    });
      this.props.toggleOverlayForm();
    } else {
      this.setState({ info: "*channel already exits" });
    }
  };
  render() {
    return (
      <div className="form-container">
        <form className="form" onSubmit={this.add}>
          <FormTitle label="Add Channel"> </FormTitle>
          <Label label="NAME" />
          <TextInput
            placeholder="Enter channel's name"
            name="name"
            onChangeAction={e => this.onHandleChange(e)}
            value={this.props.name || ""}
          />
          <Label label="PURPOSE" />
          <TextInput
            placeholder="Enter purpose of channel"
            name="purpose"
            onChangeAction={e => this.onHandleChange(e)}
            value={this.props.purpose || ""}
          />
          <SubmitButton name="add" title="Add Channel" />
          <Button
            title="Cancel"
            handleButtonClick={this.props.toggleOverlayForm}
          />
          <p className="center info">{this.state.info}</p>
        </form>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    email: state.user.email,
    name: state.channels.name,
    purpose: state.channels.purpose,
    channelList: state.channels.list
  };
}
export default connect(
  mapStateToProps,
  {
    onFieldChange,
    toggleOverlayForm,
    addChannel,
    getChannelList,
    joinChannel,
    getJoinedChannels,
    setActiveChannel,
    getMessages,
    resetAddChannelForm
  }
)(AddChannelForm);
