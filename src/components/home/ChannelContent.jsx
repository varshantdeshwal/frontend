import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "../common";
import { joinChannel, getChannelList, getMessages } from "../../actions";
import ChatBox from "./ChatBox";
class ChannelContent extends Component {
   state = {};

   render() {
      return (
         <React.Fragment>
            {this.props.channelList.length === 0 ? (
               <div className="center no-channel-message">
                  Add Channels to start discussions
               </div>
            ) : !this.props.channelName ? (
               <div className="center ">
                  <p className=" channel-title">Select Channel</p>
               </div>
            ) : (
               <React.Fragment>
                  {" "}
                  <div className="center ">
                     <p className=" channel-title">
                        {" "}
                        Welcome to {this.props.channelName}
                     </p>
                  </div>
                  {this.props.joinedChannels.includes(
                     this.props.channelName
                  ) ? (
                     <ChatBox />
                  ) : (
                     <Button
                        title="Join Channel"
                        style={{ background: "#08c" }}
                        handleButtonClick={async () => {
                           await this.props.joinChannel(
                              this.props.channelName,
                              this.props.email
                           );
                           await this.props.getChannelList();
                           await this.props.getMessages(this.props.channelName);
                        }}
                     />
                  )}
               </React.Fragment>
            )}
         </React.Fragment>
      );
   }
}
function mapStateToProps(state) {
   return {
      channelList: state.channels.list || [],
      channelName: state.channels.activeChannel,
      joinedChannels: state.user.channels,
      email: state.user.email
   };
}
export default connect(
   mapStateToProps,
   { joinChannel, getChannelList, getMessages }
)(ChannelContent);
