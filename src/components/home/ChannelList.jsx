import React, { Component } from "react";
import { connect } from "react-redux";
import io from "socket.io-client";
import { Link } from "react-router-dom";
import { toggleOverlayForm, setActiveChannel, getChannelList, getJoinedChannels } from "../../actions";
class ChannelList extends Component {
   constructor(props){
      super(props);
      this.socket = io('localhost:3048');
      this.socket.on('GET_CHANNEL', function(data){
         props.getChannelList();
        
      });
   }
   render() {
      return (
         <div className="channels">
            <div className="add-channel">
               <div>Channels</div>
               <i
                  className="fas fa-plus-circle"
                  onClick={this.props.toggleOverlayForm}
               />
            </div>
            {/* <div className="channel-type">-Joined channels</div> */}
            <div className="channel-list">
               {this.props.joinedChannels.map(key => {
                  return (
                     <Link
                        to={"/home/" + key}
                        className="channel-link"
                        onClick={() => this.props.setActiveChannel(key, true)}
                     >
                        <div
                           className={
                              this.props.activeChannel === key
                                 ? "active-channel joined"
                                 : "passive-channel joined"
                           }
                        >
                           {" "}
                           {"# " + key}
                        </div>
                     </Link>
                  );
               })}
            </div>
            <div className="channel-type">-Other channels</div>
            <div className="channel-list">
               {this.props.channelList.map(key => {
                  return !this.props.joinedChannels.includes(key) ? (
                     <Link
                        to={"/home/" + key}
                        className="channel-link"
                        onClick={() => this.props.setActiveChannel(key, false)}
                     >
                        <div
                           className={
                              this.props.activeChannel === key
                                 ? "active-channel"
                                 : "passive-channel"
                           }
                        >
                           {" "}
                           {"# " + key}
                        </div>
                     </Link>
                  ) : null;
               })}
            </div>
         </div>
      );
   }
}

function mapStateToProps(state) {
   return {
      channelList: state.channels.list || [],
      activeChannel: state.channels.activeChannel,
      joinedChannels: state.user.channels || [],
      email:state.user.email
   };
}
export default connect(
   mapStateToProps,
   { toggleOverlayForm, setActiveChannel, getChannelList, getJoinedChannels }
)(ChannelList);
