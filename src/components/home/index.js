import React, { Component } from "react";
import { connect } from "react-redux";
import Dashboard from "./Dashboard";
import {
  populateStateFromLocal,
  getChannelList,
  getJoinedChannels,
  setActiveChannel
} from "../../actions";
import ChannelContent from "./ChannelContent";
import AddChannelForm from "./AddChannelForm";
class Home extends Component {
  state = {};
  componentDidMount() {
    this.props.populateStateFromLocal(this.props.history);
    this.props.getChannelList();
    this.props.getJoinedChannels(localStorage.getItem("email"));
  }
  render() {
    return (
      <div className="home-container">
        {this.props.displayOverlayForm ? <AddChannelForm /> : null}
        <div className="dash-container">
          <Dashboard />
        </div>
        <div className="chat-container">
          <ChannelContent />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    displayOverlayForm: state.overlayForm
  };
}
export default connect(
  mapStateToProps,
  {
    populateStateFromLocal,
    getChannelList,
    getJoinedChannels,
    setActiveChannel
  }
)(Home);
