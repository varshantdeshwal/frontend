import React, { Component } from "react";
import { connect } from "react-redux";
import { onFieldChange, addMessage, getMessages } from "../../actions";
import { TextInput } from "../common";
import io from "socket.io-client";
class ChatBox extends Component {
   constructor(props){
      super(props);
      this.socket = io('localhost:3048');
      this.socket.on('RECEIVE_MESSAGE',(data)=>{
         if(data.channelName===this.props.channelName)
        this.props.getMessages(data.channelName);
     });
     
   }
   
   componentDidMount() {
      this.scrollToBottom();
    }
  
    componentDidUpdate() {
      this.scrollToBottom();
    }
  
    scrollToBottom() {
      this.el.scrollTop=this.el.scrollHeight;
    }

   onHandleChange = e => {
      this.props.onFieldChange(
         e.target.name,
         e.target.value,
         "ON_UPDATE_CHANNEL"
      );
   };
   render() {
   
      return (
         <React.Fragment>
            <div className="messages" ref={el => { this.el = el; }}>
               <div className="inner">
                  {this.props.messages.map(message => {
                     return (
                        <div className="message-container">
                           <div className="message-wrapper">
                              <div className="message">{message.text}</div>
                              <div className="sender">
                                 {"~" + message.sender}
                              </div>
                           </div>
                        </div>
                     );
                  })}
               </div>
            </div>
            <form
               onSubmit={async e => {
                  e.preventDefault();
                  
                 await this.props.addMessage(
                     this.props.message,
                     this.props.channelName,
                     this.props.name
                  );
                  this.socket.emit('SEND_MESSAGE', {
                     channelName:this.props.channelName
                 });
               }}
               className="message-form"
            >
               <TextInput
                  placeholder="Enter message"
                  name="newMessage"
                  onChangeAction={e => this.onHandleChange(e)}
                  value={this.props.message || ""}
               />
            </form>
         </React.Fragment>
      );
   }
}

function mapStateToProps(state) {
   return {
      message: state.channels.newMessage,
      messages: state.channels.messages || [],
      channelName: state.channels.activeChannel,
      name: state.user.name
   };
}
export default connect(
   mapStateToProps,
   { onFieldChange, addMessage, getMessages }
)(ChatBox);
