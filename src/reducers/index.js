import { combineReducers } from "redux";
import user from "./user";
import overlayForm from "./overlayForm";
import channel from "./channel";
export default combineReducers({
   user,
   overlayForm,
   channels: channel
});
