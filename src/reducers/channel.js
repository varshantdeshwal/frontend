const initialState = { list: [] };
export default (state = initialState, action) => {
   switch (action.type) {
      case "ON_UPDATE_CHANNEL":
         return {
            ...state,
            ...action.data
         };
      default:
         return state;
   }
};
