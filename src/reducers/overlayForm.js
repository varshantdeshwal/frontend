const initialState = false;
export default (state = initialState, action) => {
   switch (action.type) {
      case "TOGGLE_OVERLAY_FORM":
         return !state;
      default:
         return state;
   }
};
