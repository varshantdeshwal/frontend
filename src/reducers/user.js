const initialState = { auth: false };
export default (state = initialState, action) => {
  switch (action.type) {
    case "ON_UPDATE_USER":
      return { ...state, ...action.data };
    default:
      return state;
  }
};
