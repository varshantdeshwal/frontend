import React, { Component, Fragment } from "react";
import {
   Route,
   BrowserRouter as Router,
   Switch,
   Redirect
} from "react-router-dom";
import { StartScreen, SigninForm, SignupForm, Home } from "./components";

class App extends Component {
   render() {
      return (
         <Fragment>
            <Router>
               <Switch>
                  <Route exact path="/" render={() => <StartScreen />} />
                  <Route
                     path="/signin"
                     render={() => {
                        return localStorage.getItem("auth") ? (
                           <Redirect to="/home" />
                        ) : (
                           <SigninForm />
                        );
                     }}
                  />
                  <Route
                     path="/signup"
                     render={() => {
                        return localStorage.getItem("auth") ? (
                           <Redirect to="/home" />
                        ) : (
                           <SignupForm />
                        );
                     }}
                  />
                  <Route path="/home" component={Home} />
               </Switch>
            </Router>
         </Fragment>
      );
   }
}

export default App;
